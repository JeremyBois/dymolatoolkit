#! /usr/bin/env python
# -*- coding:Utf8 -*-


import os

from setuptools import setup, find_packages


def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


setup(
    name='DymTK',
    version='0.1.6',
    author='Jérémy Bois',
    author_email='jeremy.bois@u-bordeaux.fr',
    description='Pree-compute/Run/Post-compute multiple Dymola simulations in parallels and easy way.',
    long_description=read('README.md'),
    keywords='modelica dymola mat morris parametric',
    license='WTFPL',
    packages=find_packages(),
    install_requires=['path.py',
                      'DyMat>=0.7',
                      'numpy',
                      'pandas>=0.18',
                      'matplotlib>=1.5',
                      'SALib>=0.7',
                      'buildingspy>=1.6'],
    package_data={'DymTK': ['config.json.*']},
    classifiers=['Development Status :: Alpha',
                 'Environment :: Console',
                 'Programming Language :: Python :: 2.7',
                 'Programming Language :: Python :: 3',
                 'Programming Language :: Python :: 3.3',
                 'Programming Language :: Python :: 3.4',
                 'Programming Language :: Python :: 3.5',
                 'Intended Audience :: Science/Research',
                 'Topic :: Scientific/Engineering',
                 'Topic :: Utilities']
)
