## DymTK : DYMola Tool Kit

A Python toolkit to make Dymola parallel simulations a **breeze**.
Can be used to make parametric / sensitivity / optimization analysis.


***


### Installation of BuildingsPy (manually):
Python3 support is not complete yet.

Anyway, to install it, there is a couple of way :

**From pypi server:**

    pip install buildingspy


**From sources:**

If you have git install on your computer you can simply run:

    pip install git+https://github.com/lbl-srg/BuildingsPy

or manually downloading the repository, extract it and run the following command:

    pip install <Path of downloaded repository>

You can now install `DymTK` using the `pip` tool:

    pip install <Path to this folder>


***


### Why using it ?
This package provides basics components to construct a complete threaded simulator
to prepare, add pre-processing on data, run simulations, add post-processing on data
in a parallel and easy way.

Can be used to:

  - Run a large number of simulation using a `sample` file
  - Create a Morris sample
  - Analyze Morris results:
    - Normalized distances
    - Mean vs standard deviation (Default or user define helpers)
    - Absolute mean vs standard deviation (Default or user define helpers)
    - Support multiple indicator
    - Output a ready to use latex table to compare factor influence on multiple indicator
  - Keep track your work flow using a logs

Scripts from `DymTK.Scripts` module provide ready to use simulation process
with sample creation and complete logging support.


This library is based on a configuration file that can be used to define:

  - Model configuration (factors, outputs, indicators, conversion, ...)
  - Dymola configuration (start, stop, step size, flags, ...)
  - Global parameter (number of thread, output directory, ...)
  - Morris parameter (number of trajectories, number of level, ...)

This configuration file can be extend easily if needed.
An example of configuration file can be found inside the library installation folder.

  - `[Python installation folder]/Lib/site-packages/DymTK/config.json.example`
  - `[Python installation folder]/Lib/site-packages/DymTK/config.json.template`


***


### How to use it:

```
    >>> # Import function from package
    >>> from DymTK import ThreadSimulator, start_morris_analysis
    >>> # Load configuration
    >>> config_file = "D:\WorkingDir\my_config.json"
    >>> # Run simulations
    >>> start_morris_analysis(config_file, ThreadSimulator)
```
