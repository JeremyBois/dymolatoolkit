#! /usr/bin/env python
# -*- coding:Utf8 -*-

"""
    A matplotlib plotter abstract class  --> MorrisPlotter
    Plot are dynamic:
        - On key event provide:
            - `m` to zoom an ax to figure size or unzoom to previous size
            - `g` to show grid
"""

# Python2 support
from __future__ import print_function
from six import add_metaclass
try:
    xrange          # Only exists in Python2
    range = xrange  # Shadow python2 range
except NameError:
    # Using Python3 nothing to do
    pass


import abc

import matplotlib as mpl
import matplotlib.pyplot as plt
import pandas as pd

from ..Exceptions import ArgError


__all__ = ['AbstractPlotter']


@add_metaclass(abc.ABCMeta)  # Python2 and Python3 support
class AbstractPlotter(object):

    """An Abstract class which must be inherited to make plots."""
    # Text formatters
    font_title = {'size': 20,
                  'family': 'Baskerville Old Face'}
    font_legend = {'size': 15,
                   'family': 'Baskerville Old Face'}
    font_base = {'size': 15,
                 'family': 'Source Code Pro'}
    font_annotation = {'size': 15,
                       'family': 'Baskerville Old Face'}
    colormap = 'viridis'

    def __init__(self, dataframe, name=None, title='Title'):
        """Return a plotter aware of multi-axes struture.

        :param dataframe: A pandas DataFrame structure
        :param name: Name of dataframe used to transform to multi-column index.
                     Only used if dataframe is not a multi-index DataFrame.
        :param title: Window title to use

        """
        self.title = title
        # Empty before running add_fig
        self._fig = None
        self._figures = []
        self._axes = []
        # Construction of dataframe
        self._construct_dataframe(dataframe, name)

    @property
    def axes(self):
        return self._axes

    @property
    def figures(self):
        return self._figures

    @property
    def fig(self):
        return self._fig

    @fig.setter
    def fig(self, figure):
        if isinstance(figure, mpl.figure.Figure):
            self._fig = figure
        else:
            raise TypeError('*figure* must be a `matplotlib.figure.Figure` instance.')

    @abc.abstractmethod
    def figure_creator(self, **kwargs):
        """Must be overwrited to create a figure.
        Must returns a tuple of (fig_instance, axes_instance_list)
        """
        pass

    @abc.abstractmethod
    def plot(self):
        """Must be overwrited to create a plot."""
        pass

    def show(self, reset_layout=False):
        """Plot and show all figures."""
        for fig_dict in self.figures:
            fig = fig_dict['fig']
            if reset_layout:
                self.set_layout(fig)
        plt.show()

    def update(self, reset_layout=False):
        """Redraw all figure with changes applied.
        Can be used when you draw a figure but change something and want to see
        the effect.

        :param reset_layout: True means :meth:`set_layout` will be calls on
                             each figure (Default to False).

        ..seealso:`set_active_fig`
        """
        for fig_dict in self.figures:
            fig = fig_dict['fig']
            plt.close(fig)
            if reset_layout:
                self.set_layout(fig)
            fig.show()
        # plt.show(*args, **kwargs)

    def clean(self):
        """Remove all figure and corresponding windows."""
        for fig_dict in self.figures:
            plt.close(fig_dict['fig'])
        self._figures = []
        self._axes = []
        self._fig = None

    def set_active_fig(self, fig_id_or_name):
        """Change current active figure for class methods."""
        if fig_id_or_name not in range(len(self.figures)):
            fig_id = self._find_id_from_name(fig_id_or_name)
        else:
            fig_id = fig_id_or_name
        # Find figure using id
        try:
            self._fig = self._figures[fig_id]['fig']
            self._axes = self._figures[fig_id]['axes']
        except (IndexError, KeyError):
            print('Figure does not change. {} is not valid id or name'.format(fig_id_or_name))

    def add_fig(self, name, **kwargs):
        """Add a new figure to plotter.

        :param name: Unique identifier assign to this figure.
        """
        # Create a new figure with axes
        fig, axes = self.figure_creator(**kwargs)
        # Initialize axes list
        self._axes = []
        if axes is not None:
            for ax in axes:
                self._add_ax(ax)
        # Set current fig to new one and append it to figures list
        self._add_fig(fig, name)

    def set_layout(self, fig=None):
        """Can be overwritten to change figure layout before :meth:`show`."""
        self.tight_layout(fig=fig)

    def autofmt_xdate(self):
        """ Quick access to date auto formatter. """
        self.fig.autofmt_xdate()

    def tight_layout(self, fig=None, **kwargs):
        """
            Quick access to fig.tight_layout().

            Adjust subplot parameters to give specified padding.

            Parameters:

              *pad* : float
                padding between the figure edge and the edges of subplots,
                as a fraction of the font-size.
              *h_pad*, *w_pad* : float
                padding (height/width) between edges of adjacent subplots.
                Defaults to `pad_inches`.
              *rect* : if rect is given, it is interpreted as a rectangle
                (left, bottom, right, top) in the normalized figure
                coordinate that the whole subplots area (including
                labels) will fit into. Default is (0, 0, 1, 1).
        """
        fig = self.fig if fig is None else fig
        fig.tight_layout(**kwargs)

    def adjust_layout(self, top=None, bottom=None, left=None, right=None,
                      hspace=None, wspace=None):
        """ Force figure padding.

        ..note:
            :data:`None` leads to default rc parameters for each parameter.
        """
        self.fig.subplots_adjust(top=top, bottom=bottom, left=left, right=right,
                                 hspace=hspace, wspace=wspace)

    def setp(self, *args, **kwargs):
        """General function wrapping :method:`matplotlib.pyplot.setp`."""
        plt.setp(*args, **kwargs)

    def set_axlabel(self, ax_id, label, ax='y',
                    fontdict=None, labelpad=None, **kwargs):
        fontdict = self.font_legend if fontdict is None else fontdict
        if ax == 'y':
            self.axes[ax_id].set_ylabel(label, fontdict, labelpad, **kwargs)
        elif ax == 'x':
            self.axes[ax_id].set_xlabel(label, fontdict, labelpad, **kwargs)
        else:
            raise ArgError('Can only set *ax* argument to `x` or `y`')

    def set_ax_title(self, ax_id, title, loc='center', **kwargs):
        """Set ax with *ax_id* title."""
        for (prop, value) in self.font_title.items():
            if prop not in kwargs:
                kwargs[prop] = value
        self.axes[ax_id].set_title(label=title,
                                   loc=loc,
                                   **kwargs)

    def set_title(self, title, loc='center', **kwargs):
        """Set current figure title."""
        for (prop, value) in self.font_title.items():
            if prop not in kwargs:
                kwargs[prop] = value
        self.fig.suptitle(title, **kwargs)

    def set_xticklabels(self, ax_id, labels, fontdict=None, minor=False, **kwargs):
        """
            Method used to change xticks labels.
                - labels must be a list:
                    - list of list : each list will be zip and each elements of
                      list of list will be concatenate
                    - list of elements : each elements will be concatenate
                    - example : ['b', 'i']  ---> ['b', 'i']
                                [['b', 'i'], ['b', i']]  ---> ['bi', 'bi']
                - pos to get specific ax
                - fontdict to use specific text format (None to keep class default)
                - minor : False ---> major ticks, True ---> minor ticks
                - **kwargs will be passed to ax.set_xticklabels()
        """
        try:
            iter(labels[0])
        except TypeError:
            new_labs = [''.join(str(i) for i in el) for el in zip(labels)]
        else:
            # Avoid weird stuff with string and zip ...
            if isinstance(labels[0], str):
                new_labs = [''.join(str(i) for i in el) for el in zip(labels)]
            else:
                new_labs = [''.join(str(i) for i in el) for el in zip(*labels)]
        self.axes[ax_id].set_xticklabels(labels=new_labs,
                                         fontdict=self.font_base if fontdict is None else fontdict,
                                         minor=minor, **kwargs)

    def set_yticklabels(self, ax_id, labels, fontdict=None, minor=False, **kwargs):
        """
            Method used to change yticks labels.
                - labels must be a list:
                    - list of list : each list will be zip and each elements of
                      list of list will be concatenate
                    - list of elements : each elements will be concatenate
                    - example : ['b', 'i']  ---> ['b', 'i']
                                [['b', 'i'], ['b', i']]  ---> ['bi', 'bi']
                - pos to get specific ax
                - fontdict to use specific text format (None to keep class default)
                - minor : False ---> major ticks, True ---> minor ticks
                - **kwargs will be passed to ax.set_yticklabels()
        """
        try:
            iter(labels[0])
        except TypeError:
            new_labs = [''.join(str(i) for i in el) for el in zip(labels)]
        else:
            # Avoid weird stuff with string and zip ...
            if isinstance(labels[0], str):
                new_labs = [''.join(str(i) for i in el) for el in zip(labels)]
            else:
                new_labs = [''.join(str(i) for i in el) for el in zip(*labels)]
        self.axes[ax_id].set_yticklabels(labels=new_labs,
                                         fontdict=self.font_base if fontdict is None else fontdict,
                                         minor=minor, **kwargs)

    def _construct_dataframe(self, dataframe, name):
        # Make sure it’s a copy
        frame = dataframe.copy()
        # Only define for multi-index DataFrame
        if not hasattr(frame.index, 'levels'):
            iterables = ((name, ), frame.index.values)
            frame.index = pd.MultiIndex.from_product(iterables, names=['name', 'old_index'])
        # Sort to enable filtering upon dataframe later
        self.dataframe = frame.sort_index(axis=0, ascending=True)

    def _update_index(self, first, second, names=None):
        """Update index using the product of *first* and *second* as new index."""
        # Create new index
        new_index = pd.MultiIndex.from_product((first, second), names=['name', 'old_index'] if names is None else names)
        self.dataframe.index = new_index
        # Sort it again
        self.dataframe = self.dataframe.sort_index(axis=0, ascending=True)

    def _find_id_from_name(self, name):
        """Find the figure id using figure name.
        If figure no find, return None.
        """
        for fig_dict in self.figures:
            if fig_dict['name'] == name:
                return self.figures.index(fig_dict)
        return None

    def _add_ax(self, ax):
        """Add a new ax to current figure instance."""
        if isinstance(ax, mpl.axes.Axes):
            self.axes.append(ax)
        else:
            raise TypeError("*ax* must be a `matplotlib.axes.Axes` instance.")

    def _add_fig(self, fig, name):
        """Add a new reference of a figure and its axes."""
        self.fig = fig
        # Add event on click (see zoom, unzoom and on_key_m)
        self.fig.canvas.mpl_connect('key_press_event', self._on_key)
        # Add references to class instance
        self.figures.append({'fig': self.fig,
                             'name': name,
                             'axes': self.axes,
                             'ax_in_zoom': None,
                             'annotations': [],
                             'other': None})

    def _fig_index(self, figure=None):
        figure = self.fig if figure is None else figure
        for (ind, fig_dict) in enumerate(self.figures):
            if fig_dict['fig'] is figure:
                return ind
        return None

    def _zoom(self, event, index):
        """
            Zoom to full screen current selected ax.
        """
        # Get ax under mouse cursor
        selected_ax = event.inaxes
        for ax in event.canvas.figure.axes:
            if ax is not selected_ax:
                ax.set_visible(False)
        # Store ax and its old position
        self.figures[index]['ax_in_zoom'] = (selected_ax, selected_ax.get_position())
        selected_ax.set_position([0.05, 0.12, 0.9, 0.8])

    def _unzoom(self, event, index):
        """
            Unzoom previous zoom ax.
        """
        zoom_ax, ax_pos = self.figures[index]['ax_in_zoom']
        # Cancel zoom
        zoom_ax.set_position(ax_pos)
        # Make all axes visible
        for ax in event.canvas.figure.axes:
            ax.set_visible(True)
        # Reset flag to allow zoom
        self.figures[index]['ax_in_zoom'] = None

    def _on_key(self, event):
        """
            Event to run zoom or unzoom function.
        """
        # If no axes under mouse
        if event.inaxes is None:
            return
        if event.key == 'm':
            # Get figure index
            index = self._fig_index(event.inaxes.figure)
            if self.figures[index]['ax_in_zoom'] is not None:
                self._unzoom(event, index)
            else:
                self._zoom(event, index)
            event.canvas.draw()
