#! /usr/bin/env python
# -*- coding:Utf8 -*-

"""
    A collection of useful functions to manipulate json.
"""

# Python2 support
from __future__ import print_function, unicode_literals
from io import open  # Make open alias explicit for python2

import json

from path import Path

import sys
if sys.version < '3':
    convert = True
else:
    convert = False

__all__ = ['from_json', 'to_json']


def from_json(json_file, encoding='utf-8', debug=False, object_pairs_hook=None):
    """Read *json_file* and return formatted informations as
    an Dict object.
    Can return a OrderedDict object by setting *object_pairs_hook* to OrderedDict

    :param json_file: A json file **Path** storing simulation process informations.
    """
    # Read json
    with open(Path(json_file), 'r', encoding=encoding) as j_file:
        data = json.load(j_file, encoding=encoding, object_pairs_hook=object_pairs_hook)
    if debug:
        pp(data)
    # Extract config
    return data


def pp(json_dict, sort_keys=True, indent=4):
    """Json dumps wrapper to easy access to pretty print of json files.

    :param sort_keys: Sort output keys (default to True)
    :param indent: Indentation level used (default to 4)

    ..see also::`json.dump`
    """
    print(json.dumps(json_dict, sort_keys=sort_keys,
                     indent=indent, separators=(',', ': ')))


def to_json(file_path, json_dict, indent=4, encoding='utf-8'):
    """Write a dict structure to *file_path*."""
    with open(file_path, 'w', encoding=encoding) as j_file:
        output = json.dumps(json_dict, ensure_ascii=False, indent=indent)
        # Handle strange python2 behavior with empty dict {}
        if convert:
            output = unicode(output)
        j_file.write(output)


# MUST BE CHECK AND WRITE AS A GENERIC
# import pandas as pd
# def json_to_df(data_d, simulations, fields, month="Annual"):
#     """
#     Extract from *data_d* each field of *fields* for each simulation of
#     *simulations* only for the *month* parameter.
#     Return a `pandas.DataFrame` object where row index match *simulations* and **columns**
#     match *fields*.

#     :param data_d: A nested dict like object from where we want to extract data
#     :param simulations: Name of simulation to extract
#                         (must be in data_p.keys())
#     :param fields: Fields to extract for each simulation
#                    (must be in data_p[simulation][month].keys())
#     :param month: Name of the month to extract
#                   (must be in data_p[simulation].keys())
#     """
#     return pd.DataFrame({field: pd.Series(tuple((data_d[sim][month][field] for sim in simulations)),
#                                           index=simulations)
#                          for field in fields})
