#! /usr/bin/env python
# -*- coding:Utf8 -*-

"""
    A collection of useful functions to manipulate IO data format (csv, mat, DataFrame)
        - Convert DyMatFile into a DataFrame   --> to_dataframe
        - Write DataFrame to .csv              --> dataframe_to_csv
        - Convert a .mat file into a DataFrame --> mat_to_dataframe
"""

import numpy as np
import pandas as pd

from DyMat import DyMatFile
from path import Path


__all__ = ['dataframe_to_csv', 'mat_to_dataframe', 'csv_to_dataframe']


def to_dataframe(dyMatObject, variables, index_col='Time'):
    """Export DyMat dyMatObject to a pandas Dataframe, keeping only selected variables.

    :param dyMatObject: A DyMatFile instance
    :param variables: A list of variables to export to the dataframe
    :param index_col: The variable to used as index (default to 'Time').
                      index_col must be in variables or must be None.

    """
    # Used to concatenate all dataframe
    container = []
    id_max = 0
    max_len = 0
    # Variables names maps to block number
    vDict = dyMatObject.sortByBlocks(variables)
    for (idx, vList) in enumerate(vDict.values()):
        # Get values
        vData = dyMatObject.getVarArray(vList)
        # Add Time to variables names
        vList.insert(0, dyMatObject._absc[0])
        # Construct dataframe
        df = pd.DataFrame(np.transpose(vData), columns=vList)
        if len(df) > max_len:
            # Avoid duplicated column names
            try:
                container[id_max].drop('Time', 1, inplace=True)
            except IndexError:
                # Not yet a dataframe inside the container
                pass
            # Update max length and dataframe index
            max_len, id_max = len(df), idx
        else:
            # Avoid duplicated column names
            df.drop('Time', 1, inplace=True)
        container.append(df)
    # Join all and fill missing with last valid value
    frame = pd.concat(container, axis=1, join='outer')
    frame = frame.fillna(method='pad')
    return frame.set_index(index_col)


def mat_to_dataframe(mat_file, variables, index_col='Time'):
    """Load a .mat file (*mat_file*) and construct a DataFrame with it.

    :param mat_file: Path of a .mat file from Dymola or OpenModelica
    :param variables: A list of column to keep in DataFrame
    :param index_col: Column to set as index
    """
    # Load .mat file
    mat_file = Path(mat_file)
    if not mat_file.ext:
        mat_file += '.mat'
    data = DyMatFile(mat_file)
    # Return a DataFrame object
    return to_dataframe(data, variables, index_col)


def dataframe_to_csv(dataframe, path_csv, mode='w', index=True,
                     options={'sep': ';', 'float_format': '%.5f'}):
    """Export a DataFrame object (*dataframe*) as a CSV file in *path_csv* using
    *options* as csv writer/pandas configuration.
    """
    # Write .csv file
    path_csv = Path(path_csv)
    if not path_csv.ext:
        path_csv += '.csv'
    dataframe.to_csv(path_csv, mode=mode, index=index, **options)


def csv_to_dataframe(csv_path, delimiter=';', index_col=None,
                     nrows=None, skiprows=None, **kwargs):
    """Extract a pandas DataFrame from a csv file."""
    return pd.read_csv(csv_path, nrows=nrows, delimiter=delimiter,
                       skiprows=skiprows, index_col=index_col, **kwargs)
