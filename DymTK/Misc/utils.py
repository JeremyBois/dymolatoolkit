#! /usr/bin/env python
# -*- coding:Utf8 -*-


"""

    Provides:
        - Utilities (frange, create_gradiant, rect_packing)
        - Pandas utilities (rm_duplicates, ...)
        - Type converter (to_datetime, convert_to_int, ...)
        - Unit converter (to_kwh, ...)
"""
from __future__ import division

try:
    xrange          # Only exists in Python2
    range = xrange  # Shadow python2 range
except NameError:
    # Using Python3 nothing to do
    pass


import datetime
import re
import math
import numpy as np
import itertools


__all__ = ['to_celsius', 'to_kwh', 'to_l_min', 'mult_100', 'convert_to_int', 'convert_to_datetime',
           'map_to', 'apply_func_per_column', 'rm_duplicates', 'update_columns_names', 'frange',
           'binary_search', 'closest']


# Generics

def flat(iterable, out_type=tuple):
    """Return a flatten version of iterable."""
    return out_type(itertools.chain(iterable))


def binary_search(sorted_array, value):
    """A divide and conquer algorithm which allows to pick index of the first
    element larger than *value* in O(log(n)).

    :param sorted_array: A sorted iterable
    :param value: Element you want to find

    ..note:
        In case on perfect matches, return also index of first larger element.
    """
    # Fast look up
    if value >= sorted_array[-1]:
        return len(sorted_array) - 1
    left, right = 0, len(sorted_array) - 1
    while not(left > right):
        m = int(math.floor((left + right) / 2))
        if sorted_array[m] < value:
            left = m + 1
        elif sorted_array[m] > value:
            right = m - 1
        else:
            # We found a perfect match
            break
    # Weight probabilities define a range between each sorted_array element.
    # When a number is between sorted_array[0] and sorted_array[1] then we
    # want to return 1 not 0 in case of a roulette wheel.
    if not(sorted_array[m] > value):
        return m + 1
    else:
        return m


def closest(sorted_array, value):
    """Find the first element larger than *value* in O(log(n)."""
    # Get index of closest greater element
    idx = binary_search(sorted_array, value)
    # Decremental Boolean (True == 1, False == 0)
    idx -= sorted_array[idx] - value > value - sorted_array[max(0, idx - 1)]
    return sorted_array[idx]


def rect_packing(rect, triangle, til=None, degree=False):
    """
    Compute the maximum number of rectangle (define by *rect*) can pack inside a
    triangle (defines by *triangle*).
    Rectangles cannot be rotated and parameters values are as follow:

    ****************************************************************************
    *          ↑               /\                                              *
    *          |              /  \                                             *
    *          | triangle[0] /    \ triangle[1]                                *
    *          |            /      \                         _                 *
    *   Height |           /        \                   til /|  ↑              *
    *          |          /  rect[0] \                     /-|  |              *
    *          |         /   ___      \                H  /  |  |              *
    *          |        /    │ │rect[1]\                 /   |  | H_projection *
    *          ↓       /_____│_│________\               /    |  |              *
    *                   <---------------->             /_____|  ↓              *
    *                        triangle[2]                                       *
    ****************************************************************************

    :param rect: Rectangle definition as a tuple (width, height)
    :param triangle: Triangle definition as a tuple (left_side, right_side, bottom_side)
                     (Must be dimensions of projection if *til* defined)
    :param til: angle used to reverse projection.
                If *degree* is :data:`False` *til* must be in radians.
    :param degree: :data:`True if *til* in degrees else :data:`False`.

    ..note:
        Packing always start from bottom to maximize the number of rectangle to pack.
    ..note:
        Typical use is to pack solar collector on a roof section.
    ..note:
        It’s obvious that all distance must use the same dimension.
    """

    # Make sur we have a rectangle and a triangle
    assert len(rect) == 2 and len(triangle) == 3

    # Get correct triangle dimensions
    if til is not None:
        triangle = reverse_projection(triangle, til, degree)
    # Compute height using Al-Kashi theorem
    angle = math.acos((triangle[1]**2 + triangle[2]**2 - triangle[0]**2) /
                      (2 * triangle[2] * triangle[1])
                      )
    height = math.sin(angle) * triangle[1]
    # Maximum number of row
    max_level = int(math.floor(height / rect[1]))
    # Sum for each row of maximum column of panel
    pack_length = 0
    for level in range(1, max_level + 1):
        nb_cols = (height - level * rect[1]) * triangle[2] / (height * rect[0])
        pack_length += int(math.floor(nb_cols))
    return pack_length


def reverse_projection(triangle, til, degree=False):
    """Get triangle from triangle projection

    :param triangle: A 3D-tuple.
    :param til: angle used to reverse projection.
                If *degree* is :data:`False` *til* must be in radians
    :param degree: :data:`True if *til* in degrees else :data:`False`
    """
    til = til if not degree else math.radians(til)
    return tuple(triangle[index] / math.cos(til) if index != 2 else triangle[index]
                 for index in range(3))


def integrate_simpson(dataframe, column):
    """
    Integrate a column using Simpson's Rule Formula and index as time.

    :param column: Column of *dataframe* to integrate.
    :param dataframe: A pandas.DataFrame object instance

    ..warning:
        Dataframe index must be in seconds.
    """
    # Create explicite copy
    dataframe = dataframe.copy()
    # Create new column to check row number
    dataframe['number'] = np.arange(0, len(dataframe))
    ind = dataframe.index[:]
    delta_x = (ind[-1] - ind[0]) / (len(dataframe) - 1)
    boundaries = dataframe[column][ind[0]] + dataframe[column][ind[-1]]
    # Modulo and `dataframe['number']` used to sort odd and even rows number
    inside = (4 * np.sum(dataframe[column][ind[1]:ind[-1]][dataframe['number'] % 2 != 0]) +
              2 * np.sum(dataframe[column][ind[1]:ind[-1]][dataframe['number'] % 2 == 0]))
    return delta_x / 3 * (boundaries + inside)


def integrate_trapezoidal(dataframe, column):
    """
    Integration using trapezoidal shapes and index as time.

    :param column: Column of *dataframe* to integrate.
    :param dataframe: A pandas.DataFrame object instance

    ..warning:
        Dataframe index must be in seconds.
    """
    result = 0
    ind = dataframe.index[:]
    for i in range(0, len(ind) - 1):
        result = result + ((dataframe[column][ind[i + 1]] +
                            dataframe[column][ind[i]]) * (ind[i + 1] - ind[i]) / 2)
    return result


def create_gradiant(nbr_color, first=(0, 0, 0), last=(255, 255, 255), normalize=True):
    """Linear extrapolation *nbr_color* from *first* to *last* color parameter
    to create a diverging colormap."""
    if normalize:
        # Normalize color from RGB values
        first = [el / 255 for el in first]
        last = [el / 255 for el in last]
    return [tuple(f + (l - f) / nbr_color * el for (f, l) in zip(first, last)) for el in range(nbr_color)]


def frange(length):
    """Generator which returns a sequence from 0 to 1 with length elements."""
    for el in range(length):
        yield el / (length - 1)


def mult(dataframe, column, mult):
    """
    Multiply per mult values inside dataframe[column].
      - dataframe is a pandas DataFrame
      - column is a pandas DataFrame column name
    """
    dataframe[column] *= mult


def update_columns_names(dataframe, mapper):
    """Change columns name according to columns mapping inside *mapper*.

    Only update where new columns names map to old one, other remains unchanged.
    Be aware that a misspelling name in mapper will cause the column name to remains
    unchanged silently.

    :param mapper: A dict where new columns names (values) are map to
                   old columns names (keys).
    """
    # Change columns names
    for col in dataframe.columns:
        yield mapper.get(col, col)  # yield old name if not in mapper else update it


def map_to(dict_like, keys, func, inplace=False):
    """Apply *func* to each value mapping to a key in *keys*.

    :param dict_like: Dict where func will be applied
    :param keys: A list of key of *dict_like* where to apply *func*
    :param func: Function used with dict_like[key] as parameter

    """
    if inplace:
        for key in keys:
            dict_like[key] = func(dict_like[key])
    else:
        return {key: func(value) if key in keys else value for (key, value) in dict_like.items()}


def apply_func_per_column(dataframe, mapper):
    """Apply function map to regex expression where regex match
    a column name inside the *dataframe* DataFrame.

    How to use:
        dataframe = pd.DataFrame(dict(first_Energy=[1, 2, 3, 4],
                                      Energy_notCHanged=[1, 2, 3, 4]))
        # For each columns with `Energy` inside the column name ...
        mapper = dict(myFunc=re.compile('*Energy'))
        # ... Apply `myFunc` function to their values inplace
        apply_func_per_column(dataframe, mapper)

    :param dataframe: A pandas DataFrame
    :param mapper: A dict where regex expressions are map to function.
                   For instance :
                   def myFunc(dataframe, column):
                       # Inplace operation on dataframe like
                       dataframe[column] *= 100

    ..note:
        Operations occur inplace
    """
    for column in dataframe.columns:
        for (func, regex) in mapper.items():
            if re.match(regex, column):
                func(dataframe, column)
                # Only one function can be apply per column
                break


def rm_duplicates(dataframe):
    """ Return *dataframe* with duplicated rows removed.
    Always keep last one in case of duplicated values.

    :param dataframe: A pandas.DataFrame object instance
    """
    return dataframe.groupby(dataframe.index).last()


# Type converters

def convert_to_datetime(steps, start):
    """This parser convert from seconds to a real datetime format using
    a datetime as time start position.

    :param steps: An iterable which store seconds since *start*
    :param start: Datetime used for step == 0 in *steps*
    """
    for el in steps:
        yield start + datetime.timedelta(seconds=int(el))


def to_datetime(date_col, form='%d/%m/%Y %H:%M:%S'):
    """This parser convert to a real datetime format a string according to
    *form*.
    """
    for date in date_col:
        yield datetime.datetime.strptime(date, form)


def convert_to_int(parameters, selection):
    """Return a new dict with value cast to an integer for value
    map to a key in *selection*, others remaining unchanged.
    """
    return {key: int(value) if key in selection else value
            for (key, value) in parameters.items()}


# Unit Converters

def to_celsius(dataframe, column):
    """Kelvin temperature into Celsius inside dataframe[column].

    :param dataframe: A pandas.DataFrame object instance
    :param column: Column name where operation will occurs
    """
    dataframe[column] -= 273.15


def to_l_min(dataframe, column):
    """Kg/s mass flow rate into a l/min flow rate inside dataframe[column].

    Constant density used : 1000 Kg/m3.

    :param dataframe: A pandas.DataFrame object instance
    :param column: Column name where operation will occurs
    """
    return mult(dataframe, column, 60)


def to_kwh(dataframe, column):
    """Joules into KWh inside dataframe[column].

    :param dataframe: A pandas.DataFrame object instance
    :param column: Column name where operation will occurs
    """
    return mult(dataframe, column, 1 / (1000. * 3600.))


def mult_100(dataframe, column):
    """Multiply per 100 values inside dataframe[column].

    :param dataframe: A pandas.DataFrame object instance
    :param column: Column name where operation will occurs
    """
    return mult(dataframe, column, 100)
