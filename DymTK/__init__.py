#! /usr/bin/env python
# -*- coding:Utf8 -*-

__version__ = '0.1.6'
__author__ = 'Jérémy Bois'
__title__ = 'DymTK'


# Make all element accessible from a single import statement
from DymTK.Simulators import *
from DymTK.Morris import *
from DymTK.Scripts import *
from DymTK.Misc import *


import logging
try:  # Python 2.7+
    from logging import NullHandler
except ImportError:
    class NullHandler(logging.Handler):
        """Create a NullHandler."""

        def emit(self, record):
            pass

# Assign NullHandler as default root handler
logging.getLogger(__name__).addHandler(logging.NullHandler())
