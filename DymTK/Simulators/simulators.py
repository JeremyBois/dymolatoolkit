#! /usr/bin/env python
# -*- coding:Utf8 -*-


"""
    A Dymola Threaded simulator which allows to run concurrently multiple Dymola
    simulations in a thread safe interface.
        - ThreadSimulator can be used as Base class to construct a specific simulator
"""

try:
    xrange          # Only exists in Python2
    range = xrange  # Shadow Python2 range
    from Queue import Queue
except (NameError, ImportError):
    # Using Python3 nothing to do
    from queue import Queue

import concurrent.futures
from threading import Lock

import datetime
import logging

from .dymola_simulator import DymolaSimulator
from ..Misc import (to_json, convert_to_int, update_columns_names,
                    rm_duplicates, apply_func_per_column)
from ..Misc.format_tools import mat_to_dataframe, dataframe_to_csv


class ThreadSimulator(object):

    """A Provide a convenient way to run Dymola simulation in multiple thread.
    This class use a DataFrame where rows are parameters elements and columns parameters
    names.
    The argument *sample_slice* can be used to select which rows to use.
    If not provide the whole *sample* will be used.

    Some function do nothing but can be overwritten to enhance the simulator.

    :param sample: A DataFrame where each row represent a list of parameters and
                   where columns are parameters names.
    :param directory: The name of the folder used to store simulations results
    :param configuration: A dict of Dymola parameter used to control simulation configuration
                          and model configuration.
    :param cols_converter: A tuple of two dict where:
                                1 - {<old_column_name>: <new_column_name>, ...}
                                    Replace old column name with new ones
                                2 - {<function_to_apply>: <regex_expr_used_to_find_columns_names>}
                                    Apply function to columns where regex matches
    :param sample_slice: A slice of the sample to used.
                         (default to all rows).
    :param simulator: Simulator used to create and run a simulation.
                      (default to **DymTK.Simulator.simulators.DymolaSimulator**)

    ..note:
        Order of method calls that can be overwritten when using :method:`run`:
            - :method:`initialize`
            - :method:`pre_processing`
            - :method:`clean_data`
            - :method:`save_data`
            - :method:`post_processing`.
    """

    # Data lock
    dataLock = Lock()
    # Console lock
    consoleLock = Lock()
    # File lock
    fileLock = Lock()

    def __init__(self, sample, directory, configuration,
                 sample_slice=slice(None), cols_converter=({}, {}),
                 simulator=DymolaSimulator):
        # Keep only a slice of the sample.
        self._sample = sample[sample_slice]
        self._num_runs = len(self._sample)
        self.directory = directory
        self.configuration = configuration.copy()
        self._name_mapper = cols_converter[0]
        self._func_mapper = cols_converter[1]
        # Assign simulator
        self._simulator = simulator
        # Container used to store simulations informations
        self._container = {}
        # Assign logger
        self.logging = logging.getLogger(__name__)

    def run(self, *args, **kwargs):
        """Call :method:`initialize` and then start simulations using threads pool."""
        self._run(*args, **kwargs)

    def _run(self, *args, **kwargs):
        """Start simulations"""
        self.initialize(*args, **kwargs)
        self._start_pool()

    def initialize(self, *args, **kwargs):
        """Do nothing.
        Can be overwritten to add pre-processing before starting simulations.
        This function is call just before starting threads pool.
        Signature must be respected.
        """
        pass

    def _start_pool(self):
        """Take care of thread pool using configuration parameters."""
        self.logging.debug('Simulation pool started (Length={})'.format(self._num_runs))
        # Populate a queue for thread pool
        q = Queue(maxsize=self._num_runs)
        # Generator over index to get the correct row index for each simulation
        simulation_index = (ind for ind in self._sample.index)
        for nb in simulation_index:
            sample_row = self._sample.ix[nb].to_dict()
            try:
                # Apply Integer conversion to please Dymola
                sample_row = convert_to_int(sample_row, self.configuration['convert_to_int'])
            except KeyError:
                self.logging.debug('No integer conversions, continue')
            q.put((nb, sample_row, self.directory, self.configuration))

        # Save configuration used to launch simulations
        #
        # NEED FIX ?? : RESTART SIMULATION READ THIS DATA AND REWRITE FILE
        #
        json_path = self.directory / 'dymola_config.json'
        to_json(json_path, self.configuration)
        self.logging.debug('Simulation configuration saved (Path={})'.format(json_path))

        # Start workers pool and block until queue empty
        with concurrent.futures.ThreadPoolExecutor(max_workers=self.configuration['max_workers']) as threads:
            for i in range(q.qsize()):
                threads.submit(self._worker, *q.get())

    def _worker(self, sample_id, factors, simu_directory, configuration):
        """Call by each thread at start.
        Operation occurs in that order:
            - Compute pre-processing on factors before using it for simulation
            - Configure and run simulation
            - Clean simulation using :method:`clean_data`
            - Save simulation result using :method:`save_data`
            - Compute post processing on results using :method:`post_processing`
        """
        # Debug easier
        try:
            factors_and_modifiers = self.pre_processing(sample_id, factors, simu_directory, configuration)
            simulation_name = self._run_simulation(sample_id, factors_and_modifiers, simu_directory, configuration)
            frame, csv_path = self.clean_data(sample_id, simulation_name)
            self.save_data(frame, csv_path, sample_id)
            self.post_processing(frame, sample_id)
        except Exception as e:
            self.logging.error('Simulation processing finish abruptly due to: # {} # (<id={}>)'.format(e, sample_id))
        else:
            self.logging.info('Simulation processing finish without errors (<id={}>)'.format(sample_id))

    def pre_processing(self, sample_id, factors, simu_directory, configuration):
        """Can be overwritten to process additional operation on factors before
        adding it to the simulation.
        This function is called just before simulator initialization.
        It must return **factors** and respect method signature where
        **factors** is a tuple with first element defines a list of parameter
        and second element a list of modifier.
        """
        # Defaut to empty modifier list
        factors_and_modifiers = tuple((factors, ()))
        return factors_and_modifiers

    def _run_simulation(self, sample_id, factors_and_modifiers, simu_directory, configuration):
        """Create a .mos script and run it using *configuration* as parameters.
        Operation occurs in that order:
            - Create a dymola simulator and attach a name to it
            - Add pre-processing options
            - Add simulation configuration
            - Add parameters to simulation
            - Run simulation
            - Return simulation name
        """
        with ThreadSimulator.dataLock:
            directory = simu_directory / str(sample_id)
            pre_statements = [statement for statement in configuration['preprocessing']]
            model = configuration['model']
            # Construct simulation result name as model name
            # concatenate with sample row index
            sim_name = model.split('.')[-1] + '_' + str(sample_id)

        # Build simulation
        s = self._simulator(model, 'dymola', directory)

        # Add preprocessing options
        for statement in pre_statements:
            s.addPreProcessingStatement(statement)
        self.logging.debug('Preprocessing added (<id={}>)'.format(sample_id))

        # Define result file name
        s.setResultFile(sim_name)

        # Add configuration
        s.setSolver(configuration['solver'])
        s.setTolerance(configuration['tolerance'])
        s.showGUI(configuration['show_gui'])
        s.exitSimulator(configuration['exit_after_simulation'])
        s.setStartTime(configuration['start_time'])
        s.setStopTime(configuration['stop_time'])
        s.setTimeOut(configuration['setTimeOut'])
        self.logging.debug('Simulation setup (<id={}>)'.format(sample_id))

        # Extract parameters and modifiers
        parameters, modifiers = factors_and_modifiers

        # Add modifiers
        for modifier in modifiers:
            s.addModelModifier(modifier)
        self.logging.debug('Modifiers added (<id={}>)'.format(sample_id))

        # Add parameters
        s.addParameters(parameters)
        self.logging.debug('Parameters added (<id={}>)'.format(sample_id))

        # Select between setting number of intervals
        if configuration['num_intervals'] is not None:
            s.setNumberOfIntervals(configuration['num_intervals'])
        # or setting the interval output size
        elif configuration['output_interval'] is not None:
            s.setOutputInterval(configuration['output_interval'])

        # Log simulation starts
        message = 'Simulation started (<id={}>, start={}, end={})'
        self.logging.info(message.format(sample_id,
                                         configuration['start_time'],
                                         configuration['stop_time']))
        # Simulate
        start = datetime.datetime.now()
        s.simulate()

        # Log simulation ends
        delta = datetime.datetime.now() - start  # That is not CPU time for integration
        # Add delta to container
        self.store_data(sample_id, "elapsedTime", delta.total_seconds())
        self.logging.info('Simulation ended (<id={}>, elapsedTime={})'.format(sample_id, delta))

        return sim_name

    def clean_data(self, sample_id, simulation_name):
        """
            Open the data simulation result an clean it.
            Return the cleaned DataFrame.
        """
        # Get the thread .mat file path created by Dymola (without extension)
        mat_file_path = self.directory / str(sample_id) / simulation_name
        # Read the file and stores configuration outputs in a DataFrame
        # No need to lock, only this thread read this file
        try:
            raw_frame = mat_to_dataframe(mat_file_path, self.configuration['outputs'])
        except KeyError as e:
            # Error when parsing from Dymat to dataframe inside `to_dataframe`
            message = '**KeyError** raised, please check {} is a correct output name (<id={}>)'
            self.logging.error(message.format(e, sample_id))
            raise
        # Update columns name
        with ThreadSimulator.dataLock:
            if self._name_mapper:
                raw_frame.columns = [col for col in
                                     update_columns_names(raw_frame, self._name_mapper)]

        # Remove duplicated rows in raw_frame
        raw_frame = rm_duplicates(raw_frame)
        # Cast columns values
        with ThreadSimulator.dataLock:
            if self._func_mapper:
                apply_func_per_column(raw_frame, self._func_mapper)

        self.logging.debug('Simulation results cleaned (<id={}>)'.format(sample_id))

        # Remove mat file from hard drive
        if self.configuration['delete_mat_file']:
            # No exception if file does not exist
            (mat_file_path + '.mat').remove_p()
            message = 'Simulation raw results removed (<id={}>, Path={}.mat)'
            self.logging.debug(message.format(sample_id, mat_file_path))

        # Return cleaned frame and .mat file name
        return raw_frame, mat_file_path

    def save_data(self, dataframe, csv_path, sample_id):
        """Save to a CSV simulation data cleaned.
        Only enable if **create_csv** is set to :data:`True`"""
        if self.configuration['create_csv']:
            dataframe_to_csv(dataframe, csv_path)
            message = 'Simulation cleaned results saved to CSV (<id={}>, Path={}.csv)'
            self.logging.debug(message.format(sample_id, csv_path))

    def post_processing(self, dataframe, sample_id):
        """Do nothing.
        Can be overwritten to add post-simulation processing such as computing
        morris indicators.
        """
        pass

    def store_data(self, sample_id, data_key, data):
        """Store *data* inside Simulator container to be retrieved later using
        *sample_id* and *data_key*.
        """
        with ThreadSimulator.dataLock:
            self._container.setdefault(sample_id, {})[data_key] = data

    def get_data(self, sample_id, data_key):
        """Return data store inside Simulator if exists, else raise KeyError."""
        return self._container[sample_id][data_key]
