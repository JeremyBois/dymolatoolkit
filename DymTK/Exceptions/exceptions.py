#! /usr/bin/env python
# -*- coding:Utf8 -*-


__all__ = ['ArgError']


class DymolaToolKitError(Exception):
    """Exceptions from which other will inherite."""
    pass


class ArgError(DymolaToolKitError):
    """Module exception raise when user try to add a not allowed argument."""
    pass
