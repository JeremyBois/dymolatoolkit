#! /usr/bin/env python
# -*- coding:Utf8 -*-

"""
    Collection of function for Morris analysis:
        - Start a new morris analysis  --> start_morris_analysis
        - Continue a morris analysis   --> resume_morris_analysis
"""

from path import Path

from ..Misc import (add_file_handler, from_json, to_json, map_to,
                    csv_to_dataframe, log_basicConfig, add_stream_handler)
from ..Morris import create_morris_sample, save_sample_init


def start_morris_analysis(config_path, SimulatorClass,
                          sample_slice=slice(None), cols_converter=({}, {})):
    """ Main entry which can be used to run a complete simulation process.
        - Extract configuration from configuration file at *config_path*
        - Prepare outputs folder
        - Create Morris sample
        - Save problem, Morris parameters used, Dymola parameters used
        - Start workers (as many as define in config_file)
        - Create a new log file

    :param config_path: Path of the configuration file used to define the problems
                        (Must follows the **config.json.example** template).
    :param SimulatorClass: Class used to construct simulators threads
                           (see `DymTK.Simulator.simulation` for more)
    :param sample_slice: Sample slice which will be used by workers.
    :param cols_converter: A tuple of two dict where:
                                1 - {<old_column_name>: <new_column_name>, ...}
                                    Replace old column name with new ones
                                2 - {<function_to_apply>: <regex_expr_used_to_find_columns_names>}
                                    Apply function to columns where regex matches

    ..note:
        If an indicators.csv already exists inside the directory defines in configuration
        file, new results will be append to it.
    """
    # Load analysis parameters
    config_dict = from_json(config_path)

    # Convert paths
    config_dict = map_to(config_dict, ('directory', ), Path)

    # Create folder if necessary
    dir_path = config_dict['directory']
    if not dir_path.exists():
        dir_path.makedirs()

    # Initialize logging module
    log_basicConfig()

    # Add stream logger
    add_stream_handler()

    # Create Morris sample
    sample, problem, config = create_morris_sample(config_dict)

    # Save simulations informations
    save_sample_init(config_dict['directory'], sample, problem, config)

    # Add a log to write every event to *directory*
    file_name = 'simulations{}-{}-{}_log.log'.format(sample_slice.start or 0,
                                                     sample_slice.stop or sample.last_valid_index(),
                                                     sample_slice.step or 1)
    add_file_handler(config_dict['directory'] / file_name)

    # Create a thread simulator
    simulators = SimulatorClass(sample,
                                directory=config_dict['directory'],
                                configuration=config_dict['dymola'],
                                sample_slice=sample_slice,
                                cols_converter=cols_converter)

    # Get initialization arguments if exist
    init_dict = config_dict.get('init', dict())

    # Save initialization arguments
    to_json(config_dict['directory'] / 'init.json', init_dict)
    init_dict['resume'] = False

    # Starts simulations
    simulators.run(**init_dict)


def resume_morris_analysis(sim_directory, SimulatorClass,
                           sample_slice=slice(None), cols_converter=({}, {})):
    """ Main entry which can be used to continue a simulation process.
        - Extract configuration from **dymola_config** file in *sim_directory*
        - Extract Morris sample from **sample** file in *sim_directory*
        - Start workers (as many as define in **dymola_config**)
        - Create a new log file

    :param sim_directory: Simulation directory which will be used to find configuration.
    :param SimulatorClass: Class used to construct simulators threads
                           (see `DymTK.Simulator.simulation` for more)
    :param sample_slice: Sample slice which will be used by workers.
    :param cols_converter: A tuple of two dict where:
                                1 - {<old_column_name>: <new_column_name>, ...}
                                    Replace old column name with new ones
                                2 - {<function_to_apply>: <regex_expr_used_to_find_columns_names>}
                                    Apply function to columns where regex matches
    """
    # Convert sim_directory to Path object
    sim_directory = Path(sim_directory)
    # Test if path exists
    if not sim_directory.exists():
        raise NotADirectoryError('{}<sim_directory> does not exists'.format(sim_directory))

    # Extract Dymola configuration
    dymola_config = from_json(sim_directory / 'dymola_config.json')
    # Extract morris sample
    sample = csv_to_dataframe(sim_directory / 'sample.csv',
                              delimiter=';', index_col='Index')

    # Add a log to write every event to *directory*
    file_name = 'simulations{}-{}-{}_log.log'.format(sample_slice.start or 0,
                                                     sample_slice.stop or sample.last_valid_index(),
                                                     sample_slice.step or 1)
    add_file_handler(sim_directory / file_name)

    # Start threaded simulators
    simulators = SimulatorClass(sample,
                                directory=sim_directory,
                                configuration=dymola_config,
                                sample_slice=sample_slice,
                                cols_converter=cols_converter)

    # Get initialization arguments from folder
    init_dict = from_json(sim_directory / 'init.json')
    init_dict['resume'] = True

    # Starts simulations
    simulators.run(**init_dict)
