#! /usr/bin/env python
# -*- coding:Utf8 -*-

"""
    Run multiple simulations using any sample
"""

from path import Path

from ..Misc import (add_file_handler, from_json, to_json,
                    map_to, dataframe_to_csv, log_basicConfig,
                    add_stream_handler)


def run_sample(config_path, SimulatorClass, sample_func,
               sample_slice=slice(None), cols_converter=({}, {})):
    """ Main entry which can be used to run a complete simulation process.
        - Extract configuration from configuration file at *config_path*
        - Prepare outputs folder
        - Create sample using *sample_func*
        - Save sample and dymola configuration to directory extracted from config file
        - Start workers (as many as define in config_file)
        - Create a new log file inside directory

    :param config_path: Path of the configuration file used to define the problems
                        (Must follows the **config.json.example** template).
    :param sample_func: Function used to create the sample used to run simulations.
                        Must return a pandas Dataframe where index are id
                        and columns names are factors names.
    :param SimulatorClass: Class used to construct simulators threads
                           (see `DymTK.Simulator.simulation` for more)
    :param sample_slice: Sample slice which will be used by workers.
    :param cols_converter: A tuple of two dict where:
                                1 - {<old_column_name>: <new_column_name>, ...}
                                    Replace old column name with new ones
                                2 - {<function_to_apply>: <regex_expr_used_to_find_columns_names>}
                                    Apply function to columns where regex matches

    ..note:
        If an indicators.csv already exists inside the directory defines in configuration
        file, new results will be append to it.

    ..note:
        If sample_func need argument you can freeze inside a function using **functools**
        module and function **partial**.

    ..note:
        If sample.csv already exists inside the directory defines in configuration
        file, it will be overwritten.

    """
    # Load analysis parameters
    config_dict = from_json(config_path)

    # Convert paths
    config_dict = map_to(config_dict, ('directory', ), Path)

    # Create folder if necessary
    dir_path = config_dict['directory']
    if not dir_path.exists():
        dir_path.makedirs()

    # Initialize logging module
    log_basicConfig()

    # Add stream logger
    add_stream_handler()

    # Create sample and save it (override sample silently if exists)
    sample = sample_func()
    dataframe_to_csv(sample, dir_path / 'sample.csv')

    # Add a log to write every event to *directory*
    file_name = 'simulations{}-{}-{}_log.log'.format(sample_slice.start or 0,
                                                     sample_slice.stop or sample.last_valid_index(),
                                                     sample_slice.step or 1)
    add_file_handler(dir_path / file_name)

    # Create a thread simulator
    simulators = SimulatorClass(sample,
                                directory=dir_path,
                                configuration=config_dict['dymola'],
                                sample_slice=sample_slice,
                                cols_converter=cols_converter)

    # Get initialization arguments if exist
    init_dict = config_dict.get('init', {})

    # Save initialization arguments
    to_json(config_dict['directory'] / 'init.json', init_dict)

    # Starts simulations
    simulators.run(**init_dict)
