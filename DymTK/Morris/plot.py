#! /usr/bin/env python
# -*- coding:Utf8 -*-

"""
    A matplotlib plotter class to make Morris analysis a breeze  --> MorrisPlotter
    Plot are dynamic:
        - On click event provide:
            - `left` or `right` click on scatter point will enable/disable point name
        - Click on data is mirror on both morris plot (with mean and with absolute mean)
          to make analysis simple
        - On key event also provide:
            - `m` to zoom an ax to figure size or unzoom to previous size
            - `g` to show grid
        - Also add normalized distances bar plot to get more informations
"""
# Python2 support
from __future__ import division, unicode_literals
from io import open  # Make open alias explicit for python2

try:
    xrange          # Only exists in Python2
    range = xrange  # Shadow python2 range
except NameError:
    # Using Python3 nothing to do
    pass

from path import Path

import matplotlib as mpl
import matplotlib.pyplot as plt

import warnings

from ..Plotters import AbstractPlotter, AnnoteFinder
from ..Misc.utils import frange, binary_search


class MorrisPlotter(AbstractPlotter):

    colormap = 'PuBu_r'

    def __init__(self, dataframe, figsize=(20, 11),
                 name='MyIndicator', title='',
                 dyn_annotations=True, helpers=True,
                 max_annotes=8, revert_colormap=False):
        """Return a Morris specific plotter aware of multi-axes structure.

        Provide a dynamic behavior:
            - Zoom by pressing `m` key
            - Show grid using `g` key
            - Add / remove scatter points names on `click`

        Provide 4 main methods:
            - plot()    --> Create a figure for each indicator
            - show()    --> Show figures
            - update()  --> Close and show figures
            - clean()   --> Close and remove all figures

        Provide easy access all created figures
            - set_helpers()                          ---> Define helpers
            - set_active_fig()                       ---> Define active figure
            - set_ax_title(), set_yticklabels(), ... ---> Set figure parameters

        :param dataframe: A pandas DataFrame structure with multi-index
        :param figsize: Window width and height as a tuple (in inches)
        :param name: Name of indicator.
                     Only used if dataframe is not a multi-index DataFrame.
                     (Default to 'MyIndicator')
        :param title: Window title to use (different from figure title)
        :param dyn_annotations: Turn on or off dynamic annotations on click
                                (default :data:`True`)
        :param helpers: Draw lines helper on plot to make lineary analysis easier
                        (default :data:`True`)
        :param max_annotes: Maximum number of annotation to display.
                           Only used if *dyn_annotations* is :data:`False`.
        :param revert_colormap: Turn on to reverse colormap


        ..note:
            :meth:`update` can be used after updating a figure, for instance
            renaming the x axis label of a plot using :meth:`set_axlabel`.
            Update keep track of any change during the dynamics session (zoom,
            resize, ...) and re-apply them with static changes from code
            between calls of :meth:`update` or :meth:`show`.
        """
        super().__init__(dataframe, name, title)
        # Force index name
        self.dataframe.index.names = ['Indicator', 'Factor']
        # Get index for each names
        self._indicators = list(self.dataframe.index.levels[0].values)
        self._factors = list(self.dataframe.index.levels[1].values)
        self.figsize = figsize
        self.max_annotes = max_annotes
        # Set flags
        self.dyn_annotations_enabled = dyn_annotations
        self.helpers = helpers
        # Default helpers coefficient used
        self._helpers_coef = (1, 2)
        self.revert_colormap = revert_colormap

    @property
    def indicators(self):
        """Read only indicators list. Use explicit :meth:`rename_indicators`
        to update indicators names."""
        return self._indicators

    @property
    def factors(self):
        """Read only factors list. Use explicit :meth:`rename_factors`
        to update factors names."""
        return self._factors

    def rename_indicators(self, mapper):
        """Rename indicators using *mapper* dict.
        :param mapper: A dict which map new indicators names to  old ones.
        """
        # If old not in mapper just keep it unchanged
        new_indicators = tuple(mapper.get(ind, ind) for ind in self.indicators)
        # Update index
        self._update_index(indicators=new_indicators, factors=self.factors)

    def rename_factors(self, mapper):
        """Rename factors using *mapper* dict.
        :param mapper: A dict which map new factors names to old ones.
        """
        # If old not in mapper just keep it unchanged
        new_factors = tuple(mapper.get(ind, ind) for ind in self.factors)
        # Update index
        self._update_index(indicators=self.indicators, factors=new_factors)

    def figure_creator(self, title=None, figsize=None):
        """Create a new figure instance which can be access from :property:`fig`.

        ..Warning:
            :attr:`fig` reference will be set to new figure.
            :method:`set_active_fig` can be used to change current figure reference used.
        """
        # Create another figure and replace old reference with new figure reference
        figsize = self.figsize if figsize is None else figsize
        fig = plt.figure(figsize=figsize)
        # Add window title
        fig.canvas.manager.set_window_title(self.title if title is None else title)
        # Add subplot as a grid
        gs = mpl.gridspec.GridSpec(2, 2)
        axes = []
        # Top left
        axes.append(fig.add_subplot(gs[0, 0]))
        # Top right with shared y axis with Top left
        axes.append(fig.add_subplot(gs[0, 1], sharey=axes[0]))
        self.setp(axes[1].get_yticklabels(), visible=True)
        # Bottom
        axes.append(fig.add_subplot(gs[1, :]))
        self.set_layout(fig)
        return fig, axes

    def set_layout(self, fig):
        self.tight_layout(fig=fig, rect=(0.03, 0.1, 0.98, 0.93), h_pad=3, w_pad=2)

    def plot(self, indicators=None, colormap=None, revert_colormap=False):
        """Plot a new figure for each indicator in *indicators*.
        If *indicators* not provide, create a figure for each indicator in :attr:`dataframe`.

        :param indicators: A list of indicator to analyze
                           (default to :attr:`indicators`)
        :param colormap: A matplotlib colormap
                         (default to :classAttr:`colormap`)
        """
        # Assign argument value temporally
        self.revert_colormap = revert_colormap
        indicators = self.indicators if indicators is None else indicators
        for indicator in indicators:
            # Create another figure and get focus on it
            self.add_fig(name=indicator, title=indicator)
            # Create plots in new figure
            self._morris_plots(indicator, colormap)
        # Assign default
        self.revert_colormap = False

    def set_helpers(self, sigma_coefs=(1, 2)):
        """Allow to update the helpers using different coefficient for :math:`\sigma`.

        ..warning:
            At most 4 coefficients can be set.
        """
        max_coefs = 4
        if len(sigma_coefs) > max_coefs:
            message = ('Maximum coefficient exceeded.' +
                       'Keep the first fourth coefficients, skipping others')
            warnings.warn(message,
                          UserWarning)
            sigma_coefs = sigma_coefs[:max_coefs]
        self._helpers_coef = sigma_coefs

    def pivot(self, measure='dist_norm', indicators=None, factors=None):
        """Return a dataframe where rows are *factors* and columns are *indicators*.
        Values inside the dataframe comes from *measure* corresponding value.

        :param measure: Measure name ('sigma', 'mu', 'mu_star', 'dist', 'dist_norm')
        :param indicators: Indicators (as an Iterable) to keep as columns names
                           (default to all).
        :param factors: Factors to keep as index names
                        (default to all).
        """
        indicators = self.indicators if indicators is None else indicators
        factors = self.factors if factors is None else factors
        # Keep only selected indicators and factors for measure
        frame = self.dataframe.loc[(indicators, factors), :][measure]
        # Assign a column for each indicator
        frame = frame.reset_index(level='Indicator').pivot(columns='Indicator',
                                                           values=measure)
        # Remove weird columns name on top of index name
        frame.columns.name = ''
        return frame

    def qualitative_distances(self, indicators=None, factors=None,
                              bounds_markers=None):
        """Return dataframe where normalized distance can only take a value in
        [+++, ++, +, -, ] according to factor influence.

        :param indicators: Indicators (as an Iterable) to keep as columns names
                           (default to all).
        :param factors: Factors to keep as index names
                        (default to all).
        :param bounds_markers: A tuple of tuple to assign marks. Must respect
                               the following statement:
                               **len(bounds_markers[0]) == len(bounds_markers[1]) + 1**
                               Default to:
                               ((0,  0.05,   0.15,   0.3,    0.7,     1)
                                (  '',    '-',    '+',   '++',   '+++'))

        ..seealso:`pivot`
        """
        # First get
        dataframe = self.pivot(measure='dist_norm',
                               indicators=indicators,
                               factors=factors)

        # Only assign once bounds and marks
        bounds = (0, 0.05, 0.15, 0.3, 0.7, 1) if bounds_markers is None else bounds_markers[0]
        marks = ('', '-', '+', '++', '+++') if bounds_markers is None else bounds_markers[1]

        def mapper(value):
            """Find bound element closest to *value*."""
            return marks[binary_search(bounds, value) - 1]

        return dataframe.applymap(mapper)

    @staticmethod
    def qualitative_cross_table(dataframe, path, colored=True, symbols=True,
                                mode='a', color_map=None, **kwargs):
        """Create a latex table using qualitative values to color cells.
        Color must be declared inside the Latex Preamble as above
        ```
            % Used packages
            \\usepackage[table]{xcolor}
            \\usepackage{bookmaks}

            % Table colors with default color_map
            \definecolor{MostInfluent}{HTML}{586E75}
            \definecolor{Influent}{HTML}{93A1A1}
            \definecolor{LessInfluent}{HTML}{EEE8D5}
            \definecolor{PoorInfluent}{HTML}{FDF6E3}
        ```

        :param dataframe: Must be a dataframe from :meth:`qualitative_distances`.
        :param kwargs: Key arguments for **pandas.DataFrame.to_latex** function.

        ..note:
            Default color_map is (space are important):
                {" +++ ": "MostInfluent",
                 " ++ ":  "Influent",
                 " + ":   "LessInfluent",
                 " - ":   "PoorInfluent"}

        """
        path = Path(path)
        # Define color map (space in string is important)
        color_map = {' +++ ': 'MostInfluent',
                     ' ++ ': 'Influent',
                     ' + ': 'LessInfluent',
                     ' - ': 'PoorInfluent'} if color_map is None else color_map
        # Table env
        table_env = ('\\begin{table}\n',
                     '\\caption{Classement qualitatif de l’influence des facteurs pour différents indicateurs.}\n',
                     '\\end{table}\n')
        # Change template if colored to False
        template = '\\cellcolor{{{}}}{}' if colored else '{}{}'

        table_string = dataframe.to_latex(**kwargs)

        for (text, color_name) in color_map.items():
            name = color_name if colored else ''
            symbol = text if symbols else ''
            table_string = table_string.replace(text,
                                                template.format(name,
                                                                symbol.lstrip()))
        # Make indicators breakable in column
        table_string = table_string.replace('\_', ' ')

        # Add legend for indicators (right padded)
        table_string = table_string.replace('\n{}', '\n\\multicolumn{1}{r|}{Indicateur}')

        # Add table env to encapsulate tabular env
        table_string = table_env[0] + table_string
        table_string += table_env[1] + table_env[2]

        # Now write it to a file
        with open(path, mode=mode, encoding='utf-8') as file:
            file.write(table_string)

    def _morris_plots(self, indicator, colormap=None):
        self._add_plot_mu_star_sigma(0, indicator, colormap, edgecolor='none')
        self._add_plot_mu_sigma(1, indicator, colormap, edgecolor='none')
        self._add_plot_distance(2, indicator, colormap)
        message = 'Analyse de Morris pour l’indicateur : {}'
        self.set_title(message.format(indicator), loc='center')
        self._linkAnnotationFinders()

    def _add_plot_mu_sigma(self, ax_id, indicator, colormap=None, **kwargs):
        colormap = self._set_colormap(colormap)
        # Avoid too light values for scatter plot
        colormap = self._truncate_colormap(mpl.cm.get_cmap(colormap),
                                           minval=0.0, maxval=0.8, n=100)
        # Get only useful indicator
        frame = self.dataframe.loc[(indicator, slice(None)), :].ix[indicator]

        self.axes[ax_id].scatter(frame['mu'],
                                 frame['sigma'],
                                 s=frame['dist'] / max(frame['dist']) * 250,
                                 c=-frame['dist'],
                                 cmap=colormap,
                                 **kwargs)
        self._addAnnotations(frame, ('mu', 'sigma'), ax_id)

        # Add labels
        self.set_axlabel(ax_id, 'Moyenne [$\mu$]', ax='x')
        self.set_axlabel(ax_id, 'Écart type [$\sigma$]', ax='y')
        # Set the tick labels font
        for label in (self.axes[ax_id].get_xticklabels() + self.axes[ax_id].get_yticklabels()):
            label.set_fontname(self.font_base['family'])
            label.set_fontsize(self.font_base['size'])

        # Make ax pretty ...
        self.axes[ax_id].spines['top'].set_color('none')
        self.axes[ax_id].spines['right'].set_color('none')
        self.axes[ax_id].xaxis.set_ticks_position('none')
        self.axes[ax_id].yaxis.set_ticks_position('none')

        # Trunk x axis limits
        x_lim_low = min(frame['mu']) - abs(min(frame['mu'])) * 0.1
        y_lim_low = min(frame['sigma']) - abs(min(frame['sigma'])) * 0.1
        self.axes[ax_id].set_xlim(x_lim_low, max(frame['mu']) * 1.2)
        self.axes[ax_id].set_ylim(y_lim_low, max(frame['sigma']) * 1.1)

        if self.helpers:
            # Add helpers lines
            self._add_helper_line_mu(ax_id, frame['mu'].values, frame['sigma'].values)

    def _add_plot_mu_star_sigma(self, ax_id, indicator, colormap=None, **kwargs):
        colormap = self._set_colormap(colormap)
        # Avoid too light values for scatter plot
        colormap = self._truncate_colormap(mpl.cm.get_cmap(colormap),
                                           minval=0.0, maxval=0.8, n=100)
        # Get only useful indicator
        frame = self.dataframe.loc[(indicator, slice(None)), :].ix[indicator]

        self.axes[ax_id].scatter(frame['mu_star'],
                                 frame['sigma'],
                                 s=frame['dist'] / max(frame['dist']) * 250,
                                 c=-frame['dist'],
                                 cmap=colormap,
                                 **kwargs)
        self._addAnnotations(frame, ('mu_star', 'sigma'), ax_id)

        # Add labels
        self.set_axlabel(ax_id, 'Moyenne absolue [$\mu^{*}$]', ax='x')
        self.set_axlabel(ax_id, 'Écart type [$\sigma$]', ax='y')
        # Set the tick labels font
        for label in (self.axes[ax_id].get_xticklabels() + self.axes[ax_id].get_yticklabels()):
            label.set_fontname(self.font_base['family'])
            label.set_fontsize(self.font_base['size'])

        # Make ax pretty ...
        self.axes[ax_id].spines['top'].set_color('none')
        self.axes[ax_id].spines['right'].set_color('none')
        self.axes[ax_id].xaxis.set_ticks_position('none')
        self.axes[ax_id].yaxis.set_ticks_position('none')

        # Trunk x axis limits
        x_lim_low = min(frame['mu_star']) - abs(min(frame['mu_star'])) * 0.1
        y_lim_low = min(frame['sigma']) - abs(min(frame['sigma'])) * 0.1
        self.axes[ax_id].set_xlim(x_lim_low, max(frame['mu_star']) * 1.2)
        self.axes[ax_id].set_ylim(y_lim_low, max(frame['sigma']) * 1.1)

        if self.helpers:
            # Add helpers lines
            self._add_helper_lines_mu_star(ax_id, frame['mu_star'].values, frame['sigma'].values)

    def _add_plot_distance(self, ax_id, indicator, colormap, **kwargs):
        colormap = self._set_colormap(colormap)
        # Get only normalized distance for indicator and remvoe multi-indexing
        frame = self.dataframe.loc[(indicator, slice(None)), 'dist_norm'].ix[indicator]

        # Create color map based on colormap and frame length
        # cmap = mpl.cm.get_cmap(colormap)
        # Avoid too light values for scatter plot
        cmap = self._truncate_colormap(mpl.cm.get_cmap(colormap),
                                       minval=0.0, maxval=0.8, n=100)
        colors = [cmap(color) for color in frange(len(frame))]

        # Sort using columns in reversed order (larger first)
        frame.sort_values(axis=0, ascending=False, inplace=True)
        frame.plot(ax=self.axes[ax_id],
                   kind='bar',
                   color=colors,
                   **kwargs)

        # Add labels
        self.set_axlabel(ax_id, 'Distances normalisées [-]', ax='y')
        self.set_axlabel(ax_id, '', ax='x')
        # Set the tick labels font
        for label in (self.axes[ax_id].get_xticklabels() + self.axes[ax_id].get_yticklabels()):
            label.set_fontname(self.font_base['family'])
            label.set_fontsize(self.font_base['size'])

        # Make ax pretty ...
        self.set_xticklabels(ax_id, frame.index.values, ha='right', rotation=55,
                             fontdict=self.font_legend)
        self.axes[ax_id].spines['top'].set_color('none')
        self.axes[ax_id].spines['right'].set_color('none')
        self.axes[ax_id].xaxis.set_ticks_position('bottom')
        self.axes[ax_id].yaxis.set_ticks_position('none')
        # Add horizontal grid lines but below data
        self.axes[ax_id].yaxis.grid(True)
        self.axes[ax_id].set_axisbelow(True)

    def _add_helper_lines_mu_star(self, ax_id, x, y):
        """Add lines to :math:`\sigma = f(\mu^{*})` to make analysis easier."""
        x_y_bounds = max(max(x), max(y))
        x_values = [x_y_bounds * coef for coef in frange(20)]

        # Line style used for each curve
        linestyles = ('solid', 'dashed', 'dashdot', 'dotted')
        lines = []
        for (coef, style) in zip(self._helpers_coef, linestyles):
            label = '${}\sigma = \mu^{{*}}$'.format(coef if coef != 1 else '')
            line2D, = self.axes[ax_id].plot(x_values,
                                            [x / coef for x in x_values],
                                            color='#93a1a1', linestyle=style,
                                            label=label)
            lines.append(line2D)

        legend = self.axes[ax_id].legend(handles=lines,
                                         loc='upper center', ncol=1, prop=self.font_legend,
                                         frameon=True, fancybox=True, shadow=False,
                                         markerfirst=True, framealpha=0.5)
        legend.get_frame().set_edgecolor('#93a1a1')
        legend.get_frame().set_facecolor('#fdf6e3')

    def _add_helper_line_mu(self, ax_id, x, y):
        """Add line to :math:`\sigma = f(\mu)` to make analysis easier."""
        x_y_bounds = max(max(x), max(y))
        x_values = [x_y_bounds * coef for coef in frange(20)]

        label = '$\sigma = \mu$'
        line2D, = self.axes[ax_id].plot([x * -1 for x in x_values],
                                        x_values,
                                        color='#93a1a1', linestyle='solid',
                                        label=label)
        line2D, = self.axes[ax_id].plot(x_values,
                                        x_values,
                                        color='#93a1a1', linestyle='solid',
                                        label=label)
        legend = self.axes[ax_id].legend(handles=[line2D],
                                         loc='upper center', ncol=2, prop=self.font_legend,
                                         frameon=True, fancybox=True, shadow=False,
                                         markerfirst=True, framealpha=0.5)
        legend.get_frame().set_edgecolor('#93a1a1')
        legend.get_frame().set_facecolor('#fdf6e3')

    def _linkAnnotationFinders(self, figure=None):
        """Allows to link other axes together to enable dynamic annotations."""
        if self.dyn_annotations_enabled:
            figure = self.fig if figure is None else figure
            annotations = self.figures[self._fig_index()]['annotations']
            for i in range(len(annotations)):
                allButSelfAfs = annotations[:i] + annotations[i + 1:]
                annotations[i].links.extend(allButSelfAfs)

    def _dynamicAnnotations(self, x, y, annotes, ax):
        """Add an AnnoteFin der."""
        af = AnnoteFinder(x, y, annotes, ax=ax, font_dict=self.font_annotation)
        self.fig.canvas.mpl_connect('button_press_event', af)
        self.figures[self._fig_index()]['annotations'].append(af)

    def _addAnnotations(self, frame, cols_labels, ax_id, sort_by='dist'):
        """Add static or dynamic annotations using frame and cols_labels to define
        which column to used to place annotations.
            - frame[cols_labels[0]] ---> x
            - frame[cols_labels[1]] ---> y
        """
        if self.dyn_annotations_enabled:
            self._dynamicAnnotations(frame[cols_labels[0]], frame[cols_labels[1]],
                                     frame.index.values, self.axes[ax_id])
        else:
            # Select best from distance
            frame = frame.sort_values('dist', ascending=False).head(self.max_annotes)
            self._staticAnnotations(frame[cols_labels[0]], frame[cols_labels[1]],
                                    frame.index.values, self.axes[ax_id])

    def _staticAnnotations(self, x, y, annotes, ax):
        for (label, x, y) in zip(annotes, x, y):
            ax.annotate(label, size=self.font_annotation['size'],
                        family=self.font_annotation['family'],
                        xy=(x, y), xytext=(5, 8),
                        textcoords='offset points', ha='center', va='bottom')

    def _set_colormap(self, colormap=None):
        colormap = type(self).colormap if colormap is None else colormap
        if self.revert_colormap:
            colormap = colormap + '_r'
        return colormap

    def _truncate_colormap(self, cmap=None, minval=0.0, maxval=1, n=100):
        """Return a truncated version of *cmap* colormap with n elements.
        Returned cmap name is constructed using:
            "truc_" + cmap.name
        """
        cmap = type(self).colormap if cmap is None else cmap
        color_range = tuple(minval + i * (maxval - minval) for i in frange(n))
        new_cmap = mpl.colors.LinearSegmentedColormap.from_list(
            'trunc_{}'.format(cmap.name), cmap(color_range))
        return new_cmap

    def _update_index(self, indicators, factors):
        """New *indicators* and *factors* must be ordered the same way as
        existing index.
        Update index using the product of *indicators* and *factors* as new index.
        """
        # Update dataframe index using abstract class method
        super()._update_index(indicators, factors, names=(['Indicator', 'Factor']))

        # Update ref for indicators and factors
        self._indicators = list(self.dataframe.index.levels[0].values)
        self._factors = list(self.dataframe.index.levels[1].values)


class MorrisPlotter2(MorrisPlotter):
    """
        Same as :class:`MorrisPlotter` but only draw two kind of graph for each
        indicator.
            - sigma = f(mu*)
            - dist_norm = f(factor)
    """

    def figure_creator(self, title=None, figsize=None):
        """Create a new figure instance which can be access from :property:`fig`.

        ..Warning:
            :attr:`fig` reference will be set to new figure.
            :method:`set_active_fig` can be used to change current figure reference used.
        """
        # Add subplot as a grid
        fig, axes = plt.subplots(2, 1, figsize=self.figsize if figsize is None else figsize)
        axes = list(axes)
        # Top ax
        self.setp(axes[0].get_yticklabels(), visible=True)
        # Add window title
        fig.canvas.manager.set_window_title(self.title if title is None else title)
        # Assign layout
        self.set_layout(fig)

        return fig, axes

    def _morris_plots(self, indicator, colormap=None):
        """Draw only two plots."""
        self._add_plot_mu_star_sigma(0, indicator, colormap, edgecolor='none')
        self._add_plot_distance(1, indicator, colormap)
        message = 'Analyse de Morris pour l’indicateur : {}'
        self.set_title(message.format(indicator), loc='center')
        self._linkAnnotationFinders()


class MorrisPlotter1(MorrisPlotter):
    """
        Same as :class:`MorrisPlotter` but only draw one kind of graph for each
        indicator.
            - sigma = f(mu*)
    """

    def figure_creator(self, title=None, figsize=None):
        """Create a new figure instance which can be access from :property:`fig`.

        ..Warning:
            :attr:`fig` reference will be set to new figure.
            :method:`set_active_fig` can be used to change current figure reference used.
        """
        # Add subplot as a grid
        fig, axes = plt.subplots(1, 1, figsize=self.figsize if figsize is None else figsize)
        axes = [axes, ]
        # Top ax
        self.setp(axes[0].get_yticklabels(), visible=True)
        # Add window title
        fig.canvas.manager.set_window_title(self.title if title is None else title)
        # Assign layout
        self.set_layout(fig)

        return fig, axes

    def _morris_plots(self, indicator, colormap=None):
        """Draw only two plots."""
        self._add_plot_mu_star_sigma(0, indicator, colormap, edgecolor='none')
        message = 'Analyse de Morris pour l’indicateur : {}'
        self.set_title(message.format(indicator), loc='center')
        self._linkAnnotationFinders()
