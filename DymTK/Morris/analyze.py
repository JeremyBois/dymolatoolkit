#! /usr/bin/env python
# -*- coding:Utf8 -*-

"""
    Collection of function for Morris analysis:
        - Compute measures for all indicators ---> analyze_morris
"""

from __future__ import division

import pandas as pd

from path import Path
from SALib.analyze.morris import analyze as Salib_analyze

from ..Misc.format_tools import csv_to_dataframe, dataframe_to_csv
from ..Misc import from_json


def analyze_morris(simulation_folder, cols_to_rm=tuple(), save_measures=True):
    """Compute morris measures using data sample for each indicators and return a
    multi-index hierarchical **pandas.DataFrame**.

    Each measure is assign to a column name as follows:
        - mean                 --> mu
        - absolute mean        --> abs_mu
        - standard deviation   --> sigma
        - distance             --> dist
        - normalized distance  --> dist_norm

    :param simulation_folder: Directory used to find problem definition, sample, and indicators
    :param cols_to_rm: Columns in **indicators.csv** which is not an indicator.
                       Useful if we add any informations to **indicator.csv** file
                       such as creation date, time to simulate, ect. This columns
                       must be deleted before calculation of measures.
    :param save_measures: :data:`True` to save computed measures to *simulation_folder*
                          (default to :data:`True`)

    ..note:
        Always return a multi-dimensional index pandas DataFrame even if only one indicator.
    """
    sim_folder = Path(simulation_folder)

    # Get indicators, sample and problem definition
    indicators = csv_to_dataframe(sim_folder / 'indicators.csv', delimiter=';',
                                  index_col='Index').sort_index(axis=0, ascending=True)
    # Clean dataframe before computing measures
    for col in cols_to_rm:
        del(indicators[col])
    sample = csv_to_dataframe(sim_folder / 'sample.csv', delimiter=';',
                              index_col='Index')
    problem = from_json(sim_folder / 'problem.json')
    config = from_json(sim_folder / 'morris_config.json')

    # Indexes for a multi-index hierarchical DataFrame
    first_index = [el for el in indicators.columns]
    second_index = [el for el in problem['names']]

    # Selected measures
    measures_used = ('mu', 'mu_star', 'sigma', 'dist', 'dist_norm')

    # Compute indicators for each indicator
    dataframes = []  # Container for all indicators DataFrames
    for indicator in indicators.columns:
        # Compute measures
        results = Salib_analyze(problem, sample.values,
                                indicators[indicator].values,
                                conf_level=0.95,
                                print_to_console=False,
                                num_levels=config['num_levels'],
                                grid_jump=config['grid_jump'])
        # Create a DataFrame with it and add it to the list of concatenation
        measures_ind = pd.DataFrame(results, index=second_index,
                                    columns=measures_used)

        # Add distance measure
        measures_ind['dist'] = (measures_ind['sigma'] ** 2 +
                                measures_ind['mu_star'] ** 2) ** 0.5

        # Add normalized distance measure
        measures_ind['dist_norm'] = ((measures_ind['dist'] - measures_ind['dist'].min()) /
                                     (measures_ind['dist'].max() - measures_ind['dist'].min()))

        dataframes.append(measures_ind)

    # Concatenate only once to avoid a lot of copy
    measures = pd.concat(dataframes, axis=0, join='outer',
                         names=('Indicator', 'Factor'), keys=first_index)

    # Save calculate measures
    if save_measures:
        dataframe_to_csv(measures, sim_folder / 'measures.csv')

    return measures
