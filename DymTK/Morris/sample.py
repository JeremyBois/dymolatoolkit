#! /usr/bin/env python
# -*- coding:Utf8 -*-

"""
    Collection of function for Morris:
        - Create a sample using a config file (json)             --> create_morris_sample
        - Save sampling and problems informations to to computer --> save_sample_init
"""


import pandas as pd

from SALib.sample.morris import sample as morris_sampling
from path import Path

from ..Misc import to_json
from ..Misc.format_tools import dataframe_to_csv


def create_morris_sample(config_dict):
    """Create a Morris orientation matrix using config_dict problem information
    and return:
        - sample created
        - problem definition
        - configuration used for the problem

    :param config_file: A json file storing simulation process informations.

    """
    # Create problem
    variables = list(config_dict['factors'].keys())
    bounds = [[config_dict['factors'][var]['min'],
               config_dict['factors'][var]['max']]
              for var in variables]

    problem = {'num_vars': len(variables),
               'names': variables,
               'bounds': bounds,
               'groups': None,
               'num_runs': config_dict['morris']['num_trajectories'] * (len(variables) + 1)}

    # Define morris configuration
    config = dict(config_dict['morris'])
    if config['num_levels'] % 2 != 0:
        raise ValueError('Level number (currently = {}) must be pair.'.
                         format(config['num_levels']))
    config['grid_jump'] = config['num_levels'] // 2

    # Create sample
    sample = morris_sampling(problem,
                             config['N'],
                             config['num_levels'],
                             config['grid_jump'],
                             config['num_trajectories'],
                             config['local_optimization'])

    # Cast Numpy array to panda DataFrame
    sample = pd.DataFrame(sample, columns=problem['names'])
    return sample, problem, config


def save_sample_init(simulation_folder, sample, problem, config):
    """Save *sample* to a CSV file, *problem* and *config* to respective
    json file. """
    simulation_folder = Path(simulation_folder)
    # Save sample as CSV
    sample.index.name = 'Index'
    dataframe_to_csv(sample, simulation_folder / 'sample.csv')
    # Save problem and config as JSON
    to_json(simulation_folder / 'problem.json', problem)
    to_json(simulation_folder / 'morris_config.json', config)
